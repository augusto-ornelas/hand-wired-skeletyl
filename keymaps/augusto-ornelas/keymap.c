#include QMK_KEYBOARD_H
#include "../skeletil/skeletil.h"

uint16_t get_tapping_term(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case GUI_T(KC_A):
        case GUI_T(KC_SCLN):
        case SFT_T(KC_Z):
        case SFT_T(KC_SLSH):
        case LCTL_T(KC_MINS):
        case LCTL_T(KC_RCBR):
            return TAPPING_TERM + 50;
        default:
            return TAPPING_TERM;
    }
}

// clang-format off
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [0] = LAYOUT(
	    KC_Q       , KC_W , KC_E , KC_R , KC_T         ,                   KC_Y        , KC_U  , KC_I   , KC_O   , KC_P          ,
	    GUI_T(KC_A), KC_S , KC_D , KC_F , KC_G         ,                   KC_H        , KC_J  , KC_K   , KC_L   , GUI_T(KC_SCLN),
	    SFT_T(KC_Z), KC_X , KC_C , KC_V , KC_B         ,                   KC_N        , KC_M  , KC_COMM, KC_DOT , SFT_T(KC_SLSH),
	                               MO(1), SFT_T(KC_ENT), KC_LALT, KC_RCTL, LT(3,KC_SPC), MO(2)),

    [1] = LAYOUT(
	    KC_RCTL, KC_TAB , KC_VOLD, KC_VOLU, KC_MUTE,                   KC_F1  , KC_F2  , KC_F3  , KC_F4, KC_F5 ,
	    KC_ESC , KC_GRV , KC_MPRV, KC_MPLY, KC_MNXT,                   KC_F6  , KC_F7  , KC_F8  , KC_F9, KC_F10,
	    KC_LSFT, KC_BRID, KC_BRIU, KC_INS , RESET  ,                   KC_F11 , KC_F12 , KC_PSCR, KC_NO, KC_LSFT,
	                               KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_LALT, KC_TRNS),

    [2] = LAYOUT(
	    OSM(MOD_LCTL), RCTL(KC_BSPC), KC_END       , KC_DLR       , KC_NO        ,                   KC_NO  , KC_PGDN , KC_PGUP, KC_BSLS, KC_NO  ,
	    KC_HOME      , RCTL(KC_DEL) , KC_DEL       , KC_RGHT      , RCTL(KC_RGHT),                   KC_BSPC, KC_DOWN , KC_UP  , KC_DQUO, KC_QUOT,
	    KC_NO        , KC_NO        , OSM(MOD_LGUI), RCTL(KC_LEFT), KC_LEFT      ,                   KC_MS_L, KC_MS_D , KC_MS_U, KC_MS_R, KC_BTN1,
	                                                 KC_RCTL      , KC_TRNS      , KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS),

    [3] = LAYOUT(
	    KC_EXLM, KC_AT  , KC_HASH, KC_DLR , KC_PERC,                   KC_CIRC, KC_AMPR , KC_ASTR, KC_LPRN, KC_RPRN,
	    KC_1   , KC_2   , KC_3   , KC_4   , KC_5   ,                   KC_6   , KC_7    , KC_8   , KC_9   , KC_0   ,
	    KC_UNDS, KC_MINS, KC_PLUS, KC_EQL , KC_TILD,                   KC_PIPE, KC_LCBR , KC_LBRC, KC_RBRC, KC_RCBR,
	                               KC_RCTL, KC_LGUI, KC_LALT, KC_TRNS, KC_TRNS, KC_TRNS),

};
// clang-format on
