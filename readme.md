# Hand-wired skeletyl

*A hand-wired version of the [skeletyl](https://bastardkb.com/skeletyl/)*

![skeletyl](images/done.jpg)

> DISCLAIMER: If you want to build your keyboard in a more convenient way I recommend you to buy the whole kit from [Bastardkb](https://bastardkb.com/skeletyl/)

## Materials

* 3D printed [case](https://github.com/Bastardkb/Skeletyl)
* 3D printed [supports](#supports) included in this repository
* Cables
* 36 MX compatible switches
* 36 keycaps
* 36 diodes 1N4148 200mA
* 2 [Elite C](https://deskthority.net/wiki/Elite-C) micro controller
* 2 TRRS jacks
* Male to male TRRS cable (to connect the two halves)
* 1 resistor between 2.2kΩ and 10kΩ for the [I²C wiring](https://docs.qmk.fm/#/feature_split_keyboard?id=i2c-wiring).
* 12 M4 screw insert, M4 X D6.0 X L5.0
* 12 M4 screws
* USB-C cable

## Supports

I designed these 3d models to keep both Elite C micro-controllers and TRRS jack in place and secured inside the case.

> DISCLAIMER: Due to the shape of the support the pins in the lower part of the micro controller (B5, B7, C7, F1, F0 and B6) are no longer accessible. This wasn't a problem for me because there is more than enough pins left to setup the keyboard matrix, but if you want to use more pins for something like leds this might be a problem.

![Support](images/elite-c-adapter.jpg)

This repository contains both [left](left-elite-c-support.stl) and [right](right-elite-c-support.stl) (You can't just mirror them because the pins for the TRRS jack would not fit).

## Assembly

* [Install the screw inserts](https://docs.bastardkb.com/hc/en-us/articles/4415744775570-Prepare-the-case)
* Install the switches
* Solder the diodes in a row to column configuration.

   As you can see in the image bellow the black extreme of the diode is facing away from the switch and the current only flows from the rows to the columns. For more details read the [QMK documentation](https://docs.qmk.fm/#/how_a_matrix_works)
   ![Rows soldered](images/rows-soldered.jpg)
* Solder the columns together
   ![Adding columns](images/rows-and-cols-soldered.jpg)
* Solder the pull-up resistor following the diagram in the [documentation](https://docs.qmk.fm/#/feature_split_keyboard?id=i2c-wiring)
* Solder the rows and columns to the appropriate pins.

   This is defined in [config.h](config.h) `MATRIX_ROW_PINS` and `MATRIX_COL_PINS`
* Assemble everything together and your done!

> NOTE: when trying one half of your keyboard I²C wiring remove or comment the line `#define USE_I2C` on the file `config.h` because without the pull-up resistor the keys are not registered correctly. Add it again once you can connect both halves.

## Manual Installation

After [getting](https://docs.qmk.fm/#/newbs_getting_started) QMK clone this repository in your `qmk_firmware/keyboards/`.

The following commands compile and flash your keymap onto your keyboard.

    qmk compile -kb skeletyl -km <your-keymap>
    qmk flash -kb skeletyl -km <your-keymap>

See the [build environment setup](https://docs.qmk.fm/#/getting_started_build_tools) and the [make instructions](https://docs.qmk.fm/#/getting_started_make_guide) for more information. Brand new to QMK? Start with the [Complete Newbs Guide](https://docs.qmk.fm/#/newbs).

## Extra resources

* [How keyboards work](https://docs.qmk.fm/#/how_keyboards_work)
* [A build log of a similar keyboard](https://www.youtube.com/watch?v=UerP5bxGL3c)

